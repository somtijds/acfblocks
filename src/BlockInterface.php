<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks;

interface BlockInterface
{    
    function getID() : string;

    function settings() : array;

    function prerequisites() : bool;
}

