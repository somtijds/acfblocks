<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks\Registrar;

use Somtijds\AcfBlocks\Registrar;
use Somtijds\AcfBlocks\BlockInterface;

class BlockRegistrar implements Registrar
{
    public const PRIORITY = 11;

    private $blocksToRegister;

    function init(BlockInterface ...$blocks) : bool 
    {
        // Exit if ACF block function doesn't exist
        if (!function_exists('acf_register_block_type')) {
            return false;
        }

        foreach ($blocks as $block) {
            $settings = $block->settings();
            if (empty($settings['render_template'])) {
                continue;
            }
            $this->blocksToRegister[] = $block;
        }
        if (empty($this->blocksToRegister)) {
            return false;
        }

        return add_action(
            'init',
            [$this,'register'],
            self::PRIORITY
        );
    }

    public function register() : void {
        foreach ($this->blocksToRegister as $block) {
            if (!$block->prerequisites()) {
                continue;
            }
            \acf_register_block_type($block->settings());
        }
    }
}