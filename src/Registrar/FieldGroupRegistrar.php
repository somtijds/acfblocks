<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks\Registrar;

use Somtijds\AcfBlocks\Registrar;
use Somtijds\AcfBlocks\FieldGroupInterface;

use function \acf_add_local_field_group;

class FieldGroupRegistrar implements Registrar
{
    public const PRIORITY = 12;

    private $fieldGroupsToRegister;

    public function init(FieldGroupInterface ...$fieldGroups) : bool {

        if (!function_exists('acf_add_local_field_group')) {
            return false;
        }

        $this->fieldGroupsToRegister = [];

        foreach($fieldGroups as $fieldGroup) {
            if ($fieldGroup->hasValidConfig()) {
                $this->fieldGroupsToRegister[] = $fieldGroup;
            }
        }

        if (empty($this->fieldGroupsToRegister)) {
            return false;
        }

        return add_action(
            'acf/init',
            [$this, 'register'],
            self::PRIORITY
        );
    }

    public function register() : void {
        foreach($this->fieldGroupsToRegister as $fieldGroup) {
            $config = $fieldGroup->config();
            acf_add_local_field_group($config);
        }
    }

}