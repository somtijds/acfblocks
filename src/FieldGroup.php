<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks;

use Somtijds\AcfBlocks\FieldGroupInterface;

abstract class FieldGroup implements FieldGroupInterface
{
    protected $config;
    protected $hasBuilder = false;

    protected function __construct($config) 
    {
        $this->config = $config;
    }
    
    public function config() : array
    {
        return $this->config;
    }

    public function hasValidConfig() : bool
    {
        $config = $this->config();

        if (!is_array($config)) {
            return false;
        }
        foreach(['key','title','fields'] as $key) {
            if (!array_key_exists($key,$config)) {
                // @TODO throw error instead.
                return false;
            }
            if (empty($config[$key])) {
                // @TODO throw error instead.
                return false;
            }
        }
        return true;
    }

    public function hasLocation() : bool
    {
        $config = $this->config();

        return isset($config['location']) && is_array($config['location']);
    }

    public function setLocation(
        string $param,
        string $operator,
        string $value
    ) : array
    {
        $this->config['location'] = [
            [
                [
                    'param' => $param,
                    'operator' => $operator,
                    'value' => $value,
                ]
            ]
        ];
        return $this->config['location'];
    }
}