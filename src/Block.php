<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks;

abstract class Block implements BlockInterface 
{
    protected $baseSettings;
    protected $reflection;
    protected $defaultBlockViewPath;
    protected $defaultBlockViewUrl;

    public function __construct($templatePath = '') {
        $this->reflection = new \ReflectionClass($this);
        $fileName = $this->reflection->getFileName();
        $viewDir = '/view/' . static::getID();
        $this->defaultBlockViewPath = trailingslashit(
            dirname($fileName) . $viewDir
        );
        $this->defaultBlockViewUrl = trailingslashit(
            plugins_url('',$fileName) . $viewDir
        );
        $this->baseSettings = [
            'name' => static::getID(),
            'render_template' => static::getRenderTemplate($templatePath),
            'enqueue_assets' => [$this,'enqueueBlockAssets']
        ];
    }

    protected function getDefaultTemplatePath() : string 
    {
        $blockViewPath = $this->defaultBlockViewPath  . 'block.php';
        return is_readable($blockViewPath)
            ? $blockViewPath
            : '';
    }

    protected function getRenderTemplate($templatePath) : string
    {
        return is_readable($templatePath)
            ? $templatePath
            : self::getDefaultTemplatePath();
    }

    public function getID() : string {
        return $this->reflection->getShortName();
    }
    
    protected abstract function blockSettings() : array;

    public function enqueueBlockAssets() : void
    {
        $this->enqueueBlockStyles();
        $this->enqueueBlockScripts();
    }

    public function settings() : array
    {
        return array_merge(
            $this->blockSettings(),
            $this->baseSettings
        );
    }

    public function prerequisites() : bool
    {
        return true;
    }

    public function enqueueBlockStyles()
    {
        $blockStylesPath = $this->defaultBlockViewPath . 'block.css';
        $blockStylesUrl = $this->defaultBlockViewUrl . 'block.css';
        if (is_readable($blockStylesPath)) {
            wp_enqueue_style(static::getID().'-css', $blockStylesUrl);
        }
    }

    public function enqueueBlockScripts()
    {
        $blockScriptsPath = $this->defaultBlockViewPath . 'block.js';
        if (is_readable($blockScriptsPath)) {
            wp_enqueue_script(static::getID().'-js', $blockScriptsPath);
        } 
    }
}