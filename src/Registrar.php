<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks;

/**
 * Interface for classes that register functionality with ACF
 */
interface Registrar
{
    /* 
     * Initialize the class and collect data/settings/requirements for registration
     */
    function init() : bool;
    
    /*
     * The method containing the action, only added when requirements are met.
     */
    function register() : void;

}