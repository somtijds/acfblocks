<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks\FieldGroup;

use Somtijds\AcfBlocks\Block;

trait withBlockLocationTrait {
    
    private function setBlockLocation(Block $block) : void {
        if ($this->hasValidConfig() && !$this->hasLocation()) {
            $this->setLocation(
                'block',
                '==',
                'acf/' . strtolower($block->getID())
            );
        }
    }
}