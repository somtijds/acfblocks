<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks\FieldGroup;

use Somtijds\AcfBlocks\FieldGroup;

class CustomPostTypeFieldGroup extends FieldGroup
{
    use withPostTypeLocationTrait;

    public function __construct(array $config, string $postTypeSlug)
    {
        parent::__construct($config);
        $this->setPostTypeLocation($postTypeSlug);
    }
}