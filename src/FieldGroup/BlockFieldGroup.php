<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks\FieldGroup;

use Somtijds\AcfBlocks\Block;
use Somtijds\AcfBlocks\FieldGroup;

class BlockFieldGroup extends FieldGroup
{
    use withBlockLocationTrait;
    
    public function __construct(array $config, Block $block)
    {
        parent::__construct($config);
        $this->setBlockLocation($block);
    }

}