<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks\FieldGroup;

trait withPostTypeLocationTrait {
    
    private function setPostTypeLocation(string $postTypeSlug) : void {
        if ($this->hasValidConfig() && !$this->hasLocation()) {
            $this->setLocation(
                'post_type',
                '==',
                $postTypeSlug
            );
        }
    }
}