<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace Somtijds\AcfBlocks;

interface FieldGroupInterface
{
    function hasValidConfig();
    
    function config();

    function hasLocation();
    
    function setLocation(
        string $param,
        string $operator,
        string $value
    );
}